package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
	private int cols;
	private ArrayList<CellState> list;
	private int col;

	public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
		this.cols = columns;
		this.list = new ArrayList<CellState>();
		
		int entries = rows * columns;
		for(int i = 0; i < entries; i++) {
			list.add(initialState);
		}

    }

    @Override
    public int numRows() {
    	return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
		int index = getIndex(row, column);
		list.set(index, element);
    }

    private int getIndex(int row, int col) {
    	if(row >= this.numRows() || col > this.numColumns()) {
			throw new IndexOutOfBoundsException("Index is way to high");
		}
		if(row < 0 || col < 0) {
			throw new IndexOutOfBoundsException("Index is low");
		}
		
		return row * this.cols + col;
	}

	@Override
    public CellState get(int row, int column) {
		int index = getIndex(row, column);
		return this.list.get(index);

    }

    @Override
    public IGrid copy() {
    	IGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
		for(int row = 0; row < this.rows; row++) {
			for(int col = 0; col < this.cols; col++) {
				CellState value = this.get(row, col);
				newGrid.set(row, col, value);
			}
		}
		return newGrid;

    }
    
}
